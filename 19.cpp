// 19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

class Animal
{
public:

	virtual void Voice()
	{
		cout << "Voice!"<<endl;
	}
};

class Dog : public Animal
{
public:

	void Voice() override
	{
		cout<<"Woof!" << endl;
	}
};

class Cat : public Animal
{
public:

	void Voice()
	{
		cout <<"Meow"<<endl;
	}
};

class Cow : public Animal
{
public:
	

	void Voice() override
	{
		cout <<"Mooooooooo" << endl;
	}
	
};

class Owl : public Animal
{
public:
	void Voice() override
	{
		cout << "UwU" << endl; //�������
	}
};

int main()
{
	
	Animal* y[4];
	y[0] = new Dog();
	y[1] = new Cat();
	y[2] = new Cow();
	y[3] = new Owl();

	for (int i = 0; i < 4; i++)
	{
		y[i]->Voice();
	}
	
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
